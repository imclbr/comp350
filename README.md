# README #

## Documentation on Pagai Installation on Ubuntu 16.04 ##

Pagai is a software testing tool that makes use of abstract interpretation, allowing you to analyse the program’s metadata. The source can be found on [Pagai’s official website](http://pagai.forge.imag.fr/) with the last release on July 10, 2014. The modified version I am working with (implementing a few updates), can be cloned from https://imclbr@bitbucket.org/imclbr/comp350.git

The steps I have taken to install the software were:


### 1. Download the pagai from git, running the command ###

```
    $ git clone http://forge.imag.fr/anonscm/git/pagai/pagai.git
```

### 2. Look into the README.txt file and check all dependancies ###

### 3. The README file points the easiest way to meet all the requisites is by running the command ###

```
    $ ./autoinstall.sh
```

### 4. While the instalation goes on you can install some dependencies manually, which I had issues in the autoinstall. The main one was APRON, that can be downloaded via SVN ###

   * SVN

```
    $ sudo apt-get install subversion libapache2-mod-svn libapache2-svn libsvn-dev
```

   * APRON

```
    $ svn co svn://scm.gforge.inria.fr/svnroot/apron/apron/trunk apron
```

### 5. Adress Apron’s dependancies, which can be found in the README.txt ###

   * GMP - ([GMP website](https://gmplib.org/#DOWNLOAD))

```
    $ tar --lzip -xvf gmp-VERSION.tar.lz
```

   * M4

```
    $ sudo apt-get install m4
```

   * LZIP

```
    $ sudo apt-get install lzip
```

   * MPFR - ([MPFR website](http://www.mpfr.org/mpfr-current/#download))

```
    $ tar -xvf mpfr-4.0.0.tar.xz
    $ ./configure
    $ sudo make install
    OR
    $ apt-get install -y libmpfr-dev
```

   * LIBGMP3, LIBMPFR and PLL

```
    $ sudo apt-get install libgmp3-dev
    $ sudo apt-get install libmpfr-dev
    $ sudo apt-get install ppl-dev
```

   * Finishing GMP

```
    $ ./configure
    $ sudo make install
```

### 6. Install APRON ###

Inside the apron folder, run the commands
    
```
    $ ./configure -no-java
    $ sudo make install
```

There should be no errors, but if there are any, check what it says and refer to the dependencies above. After successful installation, run the following command to install a copy of apron into pagai’s external sources:

```
    $ ./configure -no-java --prefix=/path/to/pagai/external/apron
    $ make install
```

### 7. Install 3 extra libraries required by Pagai ###

   * BOOST

```
    $ sudo apt-get install libboost-dev
    $ sudo apt-get install libboost-all-dev
```

   * BISON

```
    $ sudo apt-get install libbison-dev
```

   * FLEX

```
    $ sudo apt-get install flex
```

   * CURSES

```
    $ sudo apt-get install lib32ncurses5-dev
```

### 8. Run Pagai’s autoinstall again ###

```
    $ ./autoinstall.sh
```