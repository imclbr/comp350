# Based off https://github.com/kpmeen/docker-scala-sbt

FROM ubuntu:xenial


# Install locales package
RUN apt-get update && DEBIAN_FRONTEND=noninteractive apt-get install -y locales

#Install GIT
RUN apt-get install -y git

#Install CURL
RUN apt-get install -y curl

# Download Pagai and make external folder for APRON
RUN  git clone http://forge.imag.fr/anonscm/git/pagai/pagai.git 

RUN apt-get install -y apt-utils

#Install SVN
RUN apt-get -qq install -y subversion libapache2-mod-svn libapache2-svn libsvn-dev

#Download Apron
RUN svn co svn://scm.gforge.inria.fr/svnroot/apron/apron/trunk apron

#Install GMP
RUN apt-get -qq install -y libgmp-dev

#Install M4
RUN apt-get install -y m4

#Install MPFR
RUN apt-get install -y libmpfr-dev

#Install APRON
RUN mkdir -p ~/pagai/external/apron
RUN apt-get update \
  && apt-get install -y build-essential \
  && apt-get install -y libppl-dev

WORKDIR apron
RUN ./configure -no-java --prefix /pagai/external/apron \
  && make install

#Install BOOST
RUN apt-get install -y libboost-dev libboost-all-dev

#Install BISON
RUN apt-get install -y libbison-dev

#Install FLEX
RUN apt-get install -y flex

#Install CURSES
RUN apt-get -qq install -y lib32ncurses5-dev

#Install GROFF and CMAKE
RUN apt-get install -y groff groff-base 
RUN apt-get install -y cmake

#Build Pagai
RUN cd .. \
  && cd pagai \
  && ./autoinstall.sh

#PATH for Pagai, Clang and LLVM
ENV PATH="/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/pagai/external/llvm/bin:/pagai/src/:${PATH}"

#Move working directory to where pagai is built
WORKDIR /pagai/src